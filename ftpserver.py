# Author: Cosimo Agati

# Salvo i commenti, ho preferito scrivere docstrings e nomi in inglese, per
# avere maggiore uniformita' con il linguaggio.

# E' possibile passare la porta da utilizzare per la connessione principale.
# Avviare come "python ftpserver.py <numeroporta>"

import socket
import platform
import os
import random
import subprocess


class MyFtpServer:
    def __init__(self, userdata, port):
        """Class constructor: user information and port are saved"""
        self.userdata = userdata
        self.port = int(port)
        self.init_conn_params()
        self.init_sockets()
        self.init_dicts()

    def init_conn_params(self):
        """Initializes main connection parameters"""
        self.connected = False
        self.manual_second_port = False
        self.pasv_mode_active = False
        self.second_conn_active = False
        self.pasv_ipaddr = '127.0.0.1'
        self.platform = platform.system()
        self.error_message = '530 Not logged in\r\n'
        self.transfer_type = 'L8'

    def init_sockets(self):
        """Initilizes main and secondary socket"""
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind(('', self.port))
        self.socket.listen(1)
        self.second_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def init_dicts(self):
        """Initializes dictionaries containing command methods"""
        self.cmd_dict = {'USER': self.get_user, 'PASS': self.early_pass,
                         'SYST': self.syst_cmd, 'FEAT': self.feat_cmd,
                         'PWD': self.pwd, 'NOOP': self.noop_cmd,
                         'QUIT': self.close,
                         'CWD': self.cwd_cmd, 'CDUP': self.cdup_cmd,
                         'PORT': self.port_cmd, 'LIST': self.list_cmd,
                         'PASV': self.pasv_cmd, 'TYPE': self.type_cmd,
                         'RETR': self.retr_cmd}
        self.notlogged_dict = {'USER': self.login,
                               'PASS': self.early_pass, 'QUIT': self.close}
        self.waiting_dict = {'USER': self.login, 'PASS': self.check_password,
                             'QUIT': self.close}
        self.file_transfer_options = ('A', 'E', 'I', 'L', 'AN', 'AT', 'AN',
                                      'AC', 'EN', 'ET', 'EC', 'I', 'L1', 'L2',
                                      'L3', 'L4', 'L5',
                                      'L6', 'L7', 'L8')
        # I tre dizionari rappresentano le tre modalita': il primo contiene
        # i comandi eseguibili una volta eseguito il login, il secondo quelli
        # eseguibili prima di aver effettuato il comando USER, e il terzo
        # quelli ammessi una volta effettuato il login, ma non ancora immessa
        # la password. E' inoltre inizializzata una lista contenente le
        # modalita' di trasferimento file ammesse.

    def start(self):
        """Starts the server. The main socket is created"""
        print 'FTP server started'
        self.handle_connection()

    def handle_connection(self):
        """Handles main loop. A dictionary is consulted to get commands"""
        while True:
            if not self.connected:
                self.connect()
            self.cmd_list = self.get_cmd()
            try:
                command = self.cmd_list[0].upper()
                print command
                self.current_dict[command]()
            except KeyError:
                self.client_sock.send(self.error_message)
            except IndexError:
                self.close()
        # La connessione viene chiusa se si riceve come comando una stringa
        # vuota.

    def connect(self):
        """Connects to a client"""
        self.client_sock, self.addr = self.socket.accept()
        self.client_sock.send('220 FTP server ready\r\n')
        self.connected = True
        self.current_dict = self.notlogged_dict

    def get_cmd(self):
        """Reads command and splits it, in case args are needed"""
        try:
            full_command = self.client_sock.recv(1024)
            return full_command.split()
        except KeyboardInterrupt:
            self.client_sock.send('220 FTP server closed\r\n')
            print '\nFTP server closed'
            exit()

    def login(self):
        """Handles user login"""
        if len(self.cmd_list) < 2:
            self.client_sock.send('501 Syntax error in arguments\r\n')
            return
        username = self.cmd_list[1]
        for user in self.userdata.keys():
            if username == user:
                self.login_ok(username)
                return
        if username is not None:
            self.client_sock.send('430 Invalid username\r\n')

    def login_ok(self, username):
        """Login successful: all useful user information is saved"""
        self.client_sock.send('331 Username ok, need password\r\n')
        self.user = username
        self.init_directory(username)
        self.current_dict = self.waiting_dict
        self.error_message = '331 Username ok, need password\r\n'

    def init_directory(self, username):
        self.pwd = self.userdata[username][1]
        self.rootdir = self.pwd
        if not os.path.isdir(self.rootdir):
            os.mkdir(self.rootdir)
        os.chdir(self.rootdir)

    def early_pass(self):
        """Prompts the user to log in before entering password"""
        self.client_sock.send('503 Login with USER first\r\n')

    def check_password(self):
        """Handles PASS command"""
        if len(self.cmd_list) < 2:
            self.client_sock.send('501 Missing argument\r\n')
            return
        password = self.cmd_list[1]
        correct_password = self.userdata[self.user][0]
        if password == correct_password:
            self.handle_right_password()
        else:
            self.handle_wrong_password()

    def handle_right_password(self):
        """Communicates a successful login"""
        self.client_sock.send('230 Logged in as ' + self.user + '\r\n')
        self.current_dict = self.cmd_dict
        if not os.path.isdir(self.rootdir):
            os.mkdir(self.rootdir)
        self.error_message = '500 Invalid command\r\n'

    def handle_wrong_password(self):
        """A wrong password was given: user is logged out"""
        self.current_dict = self.notlogged_dict
        self.error_message = '530 Not logged in\r\n'
        self.client_sock.send('530 Incorrect password\r\n')

    def get_user(self):
        """Handles USER command when already logged in"""
        if len(self.cmd_list) < 2:
            self.client_sock.send('501 Missing argument\r\n')
            return
        user = self.cmd_list[1]
        if user == self.user:
            self.client_sock.send('331 User ' + user
                                  + ' already logged in\r\n')
        elif user is not None:
            self.client_sock.send('530 Cannot change user after login\r\n')

    def syst_cmd(self):
        """Handles SYS command"""
        self.client_sock.send('215 ' + self.platform + ' Type: '
                              + self.transfer_type + '\r\n')

    def feat_cmd(self):
        """Handles FEAT command"""
        self.client_sock.send('211 CRLF\r\n')

    def pwd(self):
        """Handles PWD command"""
        self.client_sock.send('257 "' + self.pwd + '"\r\n')

    def noop_cmd(self):
        """Handles NOOP command"""
        self.client_sock.send('200 OK\r\n')

    def cwd_cmd(self):
        """Handles CWD command"""
        if len(self.cmd_list) < 2:
            self.client_sock.send('250 Directory unchanged\r\n')
            return
        new_pwd = self.cleanup_new_directory(self.cmd_list[1])
        if not os.path.isdir(new_pwd) or self.rootdir not in new_pwd:
            self.client_sock.send('550 Invalid directory\r\n')
        else:
            self.pwd = new_pwd
            os.chdir(self.pwd)
            self.client_sock.send('250 CWD command succsessful\r\n')
        # os.chdir sara' utile in seguito per il comando LIST.
        # Rende non necessario passare il path corrente al comando ls,
        # semplificando i controlli.

    def cleanup_new_directory(self, new_pwd):
        """Based on directory name format, change target directory"""
        prefix = self.rootdir + '/'
        if len(new_pwd) > 1 and new_pwd[-1] == '/':
            new_pwd = new_pwd[:-1]
        if new_pwd[0:2] == './':
            new_pwd = new_pwd[2:]
        if new_pwd[0] == '/':
            prefix = ''
        if new_pwd == '.':
            prefix = prefix[:-1]
            new_pwd = ''
        new_pwd = prefix + new_pwd
        return new_pwd

    def cdup_cmd(self):
        """Handles CDUP command"""
        if self.pwd == self.rootdir:
            self.client_sock.send('250 Already at root directory\r\n')
        else:
            last_slash_index = self.pwd.rfind('/')
            self.pwd = self.pwd[:last_slash_index]
            os.chdir('..')
            self.client_sock.send('250 CDUP command successful\r\n')
        # Il path della directory padre viene recuperato cancellando
        # il path corrente dalla fine fino alla prima /.
        # Naturalmente, anche os.chdir('..') e' eseguito, per mantenersi
        # nella directory corretta

    def port_cmd(self):
        """Handles PORT command"""
        try:
            args = self.cmd_list[1]
            a0, a1, a2, a3, x, y = args.split(',')
        except (ValueError, IndexError):
            self.client_sock.send('500 Error in arguments\r\n')
            return
        ipaddress = a0 + '.' + a1 + '.' + a2 + '.' + a3
        self.check_ip_and_port(ipaddress, int(x), int(y))
        self.pasv_mode_active = False
        self.open_secondary_conn()
        self.client_sock.send('200 PORT command successful\r\n')

    def check_ip_and_port(self, ipaddress, x, y):
        """Checks if secondary IP address and port are correct"""
        listaddr = ipaddress.split('.')
        listaddr = map(int, listaddr)
        if listaddr > [255, 255, 255, 255]:
            self.client_sock.send('500 Invalid IP: out of range\r\n')
        elif x not in range(4, 256) or y not in range(0, 257):
            self.client_sock.send('500 Invalid port: out of range\r\n')
        else:
            self.set_manual_port(x, y)

    def set_manual_port(self, x, y):
            self.x, self.y = x, y
            self.pasv_port = self.x * 256 + self.y
            self.manual_second_port = True

    def list_cmd(self):
        """Handles LIST command"""
        output = self.get_list()
        if output is not None:
            return
        output = ''.join(output)
        self.send_list_result(''.join(output))

    def get_list(self):
        """Based on the input, gets the correct directory list format"""
        arg_list = ['ls'] + self.cmd_list[1:]
        try:
            return subprocess.check_output(arg_list)
        except subprocess.CalledProcessError:
            self.client_sock.send('500 Error in LIST command\r\n')
        # L'uso di ls e' utilizzabile, purtroppo, solo nei sistemi UNIX-like.

    def send_list_result(self, output):
        """Sends directory list"""
        self.stream_sock.send('150 Starting communication...\r\n')
        self.client_sock.send('150 Starting communication...\r\n')
        self.stream_sock.send(output)
        self.stream_sock.send('226 Data sent successfully\r\n')
        self.client_sock.send('226 Data sent successfully\r\n')

    def pasv_cmd(self):
        """Handles PASV command"""
        if not self.manual_second_port:
            self.random_pasv_params()
        self.client_sock.send('227 Entering passive mode (' +
                              self.pasv_ipaddr.replace('.', ',') + ','
                              + str(self.x)
                              + ',' + str(self.y) + ')\r\n')
        self.pasv_mode_active = True
        self.open_secondary_conn()
        # Occhio: quel ',' fa sembrare la terza riga della stessa istruzione
        # indentata, ma non lo e'! [286-288] e' un'unico comando.
        # La stringa si prende tre righe.

    def random_pasv_params(self):
        self.x = random.randint(4, 255)
        self.y = random.randint(0, 256)
        self.pasv_port = self.x * 256 + self.y
        self.pasv_ipaddr = '127.0.0.1'

    def open_secondary_conn(self):
        self.second_sock.close()
        self.second_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.second_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        if self.pasv_mode_active:
            self.wait_pasv_response()
        else:
            self.second_sock.connect((self.pasv_ipaddr, self.pasv_port))
            self.stream_sock = self.second_sock
        self.second_conn_active = True

    def wait_pasv_response(self):
        self.second_sock.bind((self.pasv_ipaddr, self.pasv_port))
        self.second_sock.listen(1)
        self.stream_sock, self.pasv_addr = self.second_sock.accept()

    def type_cmd(self):
        """Handles TYPE command"""
        if len(self.cmd_list) < 2:
            self.client_sock.send('500 Missing argument\r\n')
            return
        option = self.cmd_list[1].upper()
        self.change_transfer_type(option)

    def change_transfer_type(self, option):
        if option not in self.file_transfer_options:
            self.client_sock.send('500 Invalid mode\r\n')
        else:
            self.transfer_type = option
            self.client_sock.send('250 Type set to ' + option + '\r\n')

    def retr_cmd(self):
        """Handles RETR command"""
        if len(self.cmd_list) < 2:
            self.client_sock.send('500 Filename required\r\n')
            return
        self.transfer_file(self.cmd_list[1])

    def transfer_file(self, filename):
        try:
            if self.transfer_type == 'I':
                curr_file = open(filename, 'rb')
            else:
                curr_file = open(filename, 'r')
            self.client_sock.send('150 Starting file transfer...\r\n')
            self.stream_sock.send('150 Starting file transfer...\r\n')
        except IOError:
            self.client_sock.send('530 Specified file does not exist\r\n')
            return
        self.stream_sock.sendall(curr_file.read())
        self.client_sock.send('226 File transfer completed\r\n')
        self.stream_sock.send('226 File transfer completed\r\n')
        # Se la modalita' di trasferimento e' "I" si legge il file in binario.

    def close(self):
        """Handles QUIT command: closes connection"""
        if self.second_conn_active:
            self.stream_sock.close()
            self.second_conn_active = False
        self.second_sock.close()
        self.client_sock.send('221 Goodbye\r\n')
        self.client_sock.close()
        self.connected = False
        self.pasv_mode_active = False
        self.current_dict = self.notlogged_dict
